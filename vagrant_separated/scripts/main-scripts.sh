useradd -m cloud -s /bin/bash
echo "cloud ALL=(ALL:ALL) NOPASSWD:ALL"  >> /etc/sudoers
mkdir -p /home/cloud/.ssh/
cat << EOF > /home/cloud/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC4tzrayU6ahMhmWuicy+oFfy//9oB+2EdbbmDfA0d+k3SpYjWVqho64/L+sQIAN0RGBJx42GkbKi8B6AriPw8omLOCk2WSYW3ymEC7n3l32M5T4cLr8LIYwoMOBZkMtRc3H62PrHgDoTJLhUOvT2ewj1SLl7iU5gQuInwPE6jWooIb8R6KMUl31qNpkafCVPz5ovw0iYbDamHQF6sq081Xl39px2345T8TofIAocyBUfCOstmAvPaD9lXIV3j9JmPhAy0oweXpxdPiQzBHXepLh/jrvHrV5ggl2iwmLgF3uzwYdFlQN6eCniBtBEcGqEacb6oP2KHfHer04WIbAMHZ eduardoefb@efb
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDFJRfiiV1n5MRWdQnTQhL1cRZtGpkl5xSmmRNZff3TAdZJnHUQbAtGj4tZqWRO57WExNJVd/q0otcvYM0RZLzWiOqB2OY/QC55KpKn9lie4lRR9QjASZktXifD8ZMLsX/qPt6oGasxCGjhGyZLwQoeopxb3ZuJtSB/3uvKLX6SqNBPF0NYs+cColY2NM0M104qaPd5KXUhoX6is8XR1GRQv2vl1I4raAp/W0xw4fKQldg1N6MWZBF2Qs0D+s6XmZJ+VouTobpMC4M/slPTEAlLG53+XNMsMvF0dYSUxCOyF9/aoywVipuDYuGwNCiRPQWTM75U+ZsbMHUHHKGignRmLHZ2Rm+KbxePrkUEX0VIbOLfpiju9Is1RkBz6hhLIk6KMIZBH+Q8H9a+smycvkcqJRquhDwweRHrMXLjvf4E5TCgEdL+Phmdsflbr78VYrKHt5t6a41FHxWHo8VPxdk6SHKOoBpjVzh7YvVqscO/C0jVJqxPxRo7wOmMI1icycM= root@gitlab
EOF

chown -R cloud:cloud /home/cloud
chmod -R 700 /home/cloud/.ssh
sed -i "s/${HOSTNAME}/${HOSTNAME}.cloudlab.int/g" /etc/hostname
#echo "ctr01.cloudlab.int" > /etc/hostname
