#!/bin/bash

# This script is for test only.
CWD=`pwd`
cp config_examples/01_all_in_one_vagrant.yml config.yml
cd vagrant_all_in_one
vagrant destroy --force
vagrant up
cd ${CWD}
python3 create_inventory.py config.yml
find -name *.j2 -exec sed -i 's/ansible_eth2/ansible_eth3/g' {} \;
find -name *.j2 -exec sed -i 's/ansible_eth1/ansible_eth2/g' {} \;
find -name *.j2 -exec sed -i 's/ansible_eth0/ansible_eth1/g' {} \;
time ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i hosts create_all_in_one.yml
#