apt install -y nginx

mkdir /etc/nginx/certs
cd /etc/nginx/certs

cd /etc/apache2/certs

domain="cloudlab.int"
openssl genrsa -out rootCA.key 2048
openssl req -x509 -new -nodes -key rootCA.key -sha256 -days 3650 -out rootCA.pem -subj "/C=BR/CN=rootca.$domain"

hname="glance"
openssl genrsa -out ${hname}.${domain}.key 2048 
openssl req -new -key ${hname}.${domain}.key -out ${hname}.${domain}.csr -subj "/C=BR/CN=$hname.$domain"


> ${domain}.ext cat <<-EOF
basicConstraints=CA:FALSE
subjectAltName = @alt_names
[alt_names]
DNS.1 = *.$domain
IP.1 = 10.100.0.10
EOF
openssl x509 -req -in ${hname}.${domain}.csr -CA rootCA.pem -CAkey rootCA.key -CAcreateserial -out ${hname}.${domain}.crt -days 825 -sha256 -extfile ${domain}.ext





cat << EOF > /etc/nginx/sites-available/glance-ssl
server {
    listen 9292 default_server ssl;
    # ssl on;
    ssl_certificate /etc/apache2/certs/glance.cloudlab.int.crt;
    ssl_certificate_key /etc/apache2/certs/glance.cloudlab.int.key;
    index index.html index.htm index.nginx-debian.html;
    server_name _;

    location / {
       proxy_pass http://127.0.0.1:9293 ;
    }
}
EOF

rm /etc/nginx/sites-enabled/glance-ssl
ln -s /etc/nginx/sites-available/glance-ssl /etc/nginx/sites-enabled/glance-ssl



systemctl restart nginx
systemctl enable nginx