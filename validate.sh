#!/bin/bash
cd /root/
source demo-openrc

function create_key(){
    # Creating the key
    echo -n "`date` Creating keypair..."
    openstack server list
    [ -f "id_rsa" ]; rm -f id_rsa;
    [ -f "id_rsa.pub" ]; rm -f id_rsa.pub;
    ssh-keygen -f id_rsa -q -N "" 

    if ! openstack keypair create --public-key id_rsa.pub ${KEY_NAME}; then echo "FAILED!"; exit 1; fi
    echo "Ok!"

    # Check if key is created
    echo -n "`date` Checking keypair..."
    if ! openstack keypair show ${KEY_NAME}    ; then echo "FAILED!"; exit 1; fi
    echo "Ok!"

}

function delete_key(){
    # Delete the key
    echo -n "`date` Deleting keypair..."
    if ! openstack keypair delete ${KEY_NAME}    ; then echo "FAILED!"; exit 1; fi
    echo "Ok!"
}

function create_provider_instance(){
    echo -n "`date` Creating instance..."
    if ! openstack server create --flavor m1.nano --image ${IMAGE_NAME} --nic net-id=`openstack network show ${PROVIDER_NETWORK_NAME} -c id | grep " id " | awk '{print $4}'` --security-group default --key-name ${KEY_NAME} ${PROVIDER_INSTANCE_NAME}; then echo "FAILED!"; exit 1; fi
    echo "Ok!"
    echo -n "`date` Checking instance..."
    if ! openstack server show ${PROVIDER_INSTANCE_NAME}; then echo "FAILED!"; exit 1; fi
    echo "Ok!"
}

function test_provider_instance(){
    c=0
    openstack security group rule create --proto icmp default 2>/dev/null
    openstack security group rule create --proto tcp --dst-port 22 default 2>/dev/null    
    while :; do
        echo -n "`date` Checkinf if VM is already active..."
        if openstack server show ${PROVIDER_INSTANCE_NAME} | grep -P '\|\s+status\s+\|\s+ACTIVE\s+\|'; then
            echo "Ok!"
            break
        fi
        sleep 10
        c=$((${c}+1))
        if [ ${c} -gt 100 ]; then
            echo "FAILED!"
            exit 1
        fi
    done
    
    instance_ip=`openstack server show ${PROVIDER_INSTANCE_NAME} | grep -oP "(?<=${PROVIDER_NETWORK_NAME}=)(\d+\.\d+\.\d+\.\d+)"`
    echo "`date`  Executing a command at VM..."

    c=0
    while :; do
        if ssh -i id_rsa -o StrictHostKeyChecking=no cirros@${instance_ip} 'uname -n'; then echo "OK"; break; fi
        sleep 10
        c=$((${c}+1))
        if [ ${c} -gt 100 ]; then
            echo "FAILED!"
            exit 1
        fi
    done
    echo "Ok!"
}

function delete_provider_instance(){
    echo -n "`date` Deleting instance..."
    if ! openstack server delete ${PROVIDER_INSTANCE_NAME}; then echo "FAILED!"; exit 1; fi
    echo "Ok!"
}

function create_self_service_network(){
    echo -n "`date` Creating self scervice network..."
    if ! openstack network create ${SELF_SERVICE_NETWORK_NAME}; then echo "FAILED"; exit 1; fi
    echo "Ok!"
    echo -n "`date` Creating self service subnet..."
    if ! openstack subnet create --network ${SELF_SERVICE_NETWORK_NAME} --dns-nameserver 8.8.8.8 --gateway 172.31.0.1 --subnet-range 172.31.0.0/24 ${SELF_SERVICE_NETWORK_NAME}; then echo "FAILED"; exit 1; fi
    echo "Ok!"
    echo -n "`date` Creating router..."
    if ! openstack router create ${ROUTER_NAME}; then echo "FAILED"; exit 1; fi
    echo "Ok!"
    echo -n "`date` Adding subnet to the router..."
    if ! openstack router add subnet ${ROUTER_NAME} ${SELF_SERVICE_NETWORK_NAME}; then echo "FAILED"; exit 1; fi
    echo "Ok!"
    echo -n "`date` Setting gateway on the router..."
    if ! openstack router set ${ROUTER_NAME} --external-gateway ${PROVIDER_NETWORK_NAME}; then echo "FAILED"; exit 1; fi
    echo "Ok!"    
}

function delete_self_service_network(){
    echo -n "`date` Clearing router..."
    if ! openstack router unset ${ROUTER_NAME} --external-gateway; then echo "FAILED"; exit 1; fi
    echo "Ok!"
    echo -n "`date` Clearing router..."
    if ! openstack router remove subnet ${ROUTER_NAME} ${SELF_SERVICE_NETWORK_NAME}; then echo "FAILED"; exit 1; fi
    echo "Ok!"
    echo -n "`date` Deleting router..."
    if ! openstack router delete ${ROUTER_NAME}; then echo "FAILED"; exit 1; fi
    echo "Ok!"
    echo -n "`date` Clearing network..."
    if ! openstack network delete ${SELF_SERVICE_NETWORK_NAME}; then echo "FAILED"; exit 1; fi
    echo "Ok!"
}

function create_selfservice_instance(){
    echo -n "`date` Creating floating IP..."
    if ! openstack floating ip create ${PROVIDER_NETWORK_NAME}; then echo "FAILED!"; exit 1; fi
    echo "Ok!"
    echo -n "`date` Creating instance..."    
    if !  openstack server create --flavor m1.nano --image ${IMAGE_NAME} --nic net-id=`openstack network show ${SELF_SERVICE_NETWORK_NAME} -c id | grep " id " | awk '{print $4}'` --security-group default --key-name ${KEY_NAME} ${SELFSERVICE_INSTANCE_NAME}; then echo "FAILED!"; exit 1; fi

    echo "Ok!"
    echo -n "`date` Checking instance..."
    if ! openstack server show ${SELFSERVICE_INSTANCE_NAME}; then echo "FAILED!"; exit 1; fi
    echo "Ok!"
}

function test_selfservice_instance(){
    c=0
    floating_ip=`openstack floating ip list | grep -oP '\d+\.\d+\.\d+\.\d+' | tail -1`
    while :; do
        echo -n "`date` Checkinf if VM is already active..."
        if openstack server show ${SELFSERVICE_INSTANCE_NAME} | grep -P '\|\s+status\s+\|\s+ACTIVE\s+\|'; then
            echo "Ok!"
            break
        fi
        sleep 10
        c=$((${c}+1))
        if [ ${c} -gt 100 ]; then
            echo "FAILED!"
            exit 1
        fi
    done
    echo "`date` Adding floating IP..."
    if ! openstack server add floating ip ${SELFSERVICE_INSTANCE_NAME} ${floating_ip}; then echo "FAILED!"; exit 1; fi
    echo "Ok!"
    echo "`date`  Executing a command at VM..."

    c=0
    while :; do
        if ssh -i id_rsa -o StrictHostKeyChecking=no cirros@${floating_ip} 'uname -n'; then echo "OK"; break; fi
        sleep 10
        c=$((${c}+1))
        if [ ${c} -gt 100 ]; then
            echo "FAILED!"
            exit 1
        fi
    done
    echo "Ok!"
}

function delete_selfservice_instance(){
    echo -n "`date` Deleting instance..."
    if ! openstack server delete ${SELFSERVICE_INSTANCE_NAME}; then echo "FAILED!"; exit 1; fi
    echo "Ok!"
    echo -n "`date` Deleting floating IP..."
    floating_ip=`openstack floating ip list | grep -oP '\d+\.\d+\.\d+\.\d+' | tail -1`
    if ! openstack floating ip delete ${floating_ip}; then echo "FAILED!"; exit 1; fi
    echo "Ok!"
}

export KEY_NAME="mykey"
export PROVIDER_INSTANCE_NAME="provider-instance"
export PROVIDER_NETWORK_NAME="clabext01"
export IMAGE_NAME="cirros"
export SELF_SERVICE_NETWORK_NAME="selfservice"
export ROUTER_NAME="router"
export SELFSERVICE_INSTANCE_NAME="selfservice"

create_key
create_provider_instance
test_provider_instance
delete_provider_instance
create_self_service_network
create_selfservice_instance
test_selfservice_instance
delete_selfservice_instance
delete_self_service_network
delete_key